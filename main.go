package main

import (
	"errors"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/bitrise-io/go-steputils/stepconf"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"
)

type config struct {
	Branch *string `env:"bitrise_git_branch"`
}

type appConfig struct {
	Name   string        `yaml:"name"`
	Config configuration `yaml:"config"`
}

type configuration struct {
	Alias   string `yaml:"app_name"`
	URL     string `yaml:"url"`
	Company string `yaml:"company"`

	Language     string `yaml:"language"`
	LanguageCode string `yaml:"language_code"`

	AndroidPackage     string `yaml:"android_package"`
	AndroidCertficates string `yaml:"android_certficates"`

	AppleAppID     string   `yaml:"apple_id"`
	AppleTeamID    string   `yaml:"team_id"`
	AppleItcTeamID string   `yaml:"itc_team_id"`
	IosPackage     string   `yaml:"ios_package"`
	IosCertficates string   `yaml:"ios_certficates"`
	IosScheme      string   `yaml:"scheme"`
	Track          string   `yaml:"track"`
	TesterGroups   string   `yaml:"tester_groups"`
	Firebase       firebase `yaml:"firebase"`
}

type firebase struct {
	AndroidAppID string `yaml:"android_app_id"`
	IosAppID     string `yaml:"ios_app_id"`
	CLIToken     string `yaml:"cli_token"`
}

type keystore struct {
	Alias    string
	Password string
}

func failf(format string, v ...interface{}) {
	fmt.Errorf(format, v...)
	os.Exit(1)
}

func main() {
	var cfg config
	if err := stepconf.Parse(&cfg); err != nil {
		fmt.Printf("Issue with input: %s", err)
	}
	stepconf.Print(cfg)
	fmt.Println()

	if cfg.Branch == nil {
		failf("Branch not provided, however this is required.")
	}

	app, err := getAppName(*cfg.Branch)
	if err != nil {
		fmt.Printf("Failed to get bump, %v", err)
		os.Exit(1)
	}

	bump, err := getBump(*cfg.Branch)
	if err != nil {
		fmt.Printf("Failed to get bump, %v", err)
		os.Exit(1)
	}

	release, err := getRelease(*cfg.Branch)
	if err != nil {
		fmt.Printf("Failed to get bump, %v", err)
		os.Exit(1)
	}

	c := getConfiguration(app)

	keystorePassword, keyAlias, keyPassword := getKeystoreCredentials(app)

	writeEnv("APP_NAME", app)
	writeEnv("CRACKNEL_BUMP", bump)
	writeEnv("CRACKNEL_RELEASE_ENVIRONMENT", release)
	writeEnv("CRACKNEL_FIREBASE_CLI_TOKEN", c.Config.Firebase.CLIToken)
	writeEnv("CRACKNEL_FIREBASE_ANDROID_APP_ID", c.Config.Firebase.AndroidAppID)
	writeEnv("CRACKNEL_FIREBASE_IOS_APP_ID", c.Config.Firebase.IosAppID)
	writeEnv("CRACKNEL_GOOGLE_PLAY_SECRETS_PATH_KEY", c.Config.AndroidCertficates)
	writeEnv("CRACKNEL_ANDROID_PACKAGE", c.Config.AndroidPackage)
	writeEnv("CRACKNEL_TESTER_GROUP", c.Config.TesterGroups)
	writeEnv("CRACKNEL_APPLE_TEAM_ID", c.Config.AppleTeamID)
	writeEnv("CRACKNEL_APPLE_ITC_TEAM_ID", c.Config.AppleItcTeamID)
	writeEnv("CRACKNEL_IOS_APP_ID", c.Config.AppleAppID)
	writeEnv("CRACKNEL_IOS_SCHEME", c.Config.IosScheme)
	writeEnv("CRACKNEL_IOS_PACKAGE", c.Config.IosPackage)
	writeEnv("CRACKNEL_IOS_CERTIFICATE", c.Config.IosCertficates)
	writeEnv("BITRISEIO_ANDROID_KEYSTORE_PASSWORD", keystorePassword)
	writeEnv("BITRISEIO_ANDROID_KEYSTORE_ALIAS", keyAlias)
	writeEnv("BITRISEIO_ANDROID_KEYSTORE_PRIVATE_KEY_PASSWORD", keyPassword)

	os.Exit(0)
}

func writeEnv(key, value string) {
	cmdLog, err := exec.Command("bitrise", "envman", "add", "--key", key, "--value", value).CombinedOutput()
	if err != nil {
		fmt.Printf("Failed to expose output with envman, error: %#v | output: %s", err, cmdLog)
		os.Exit(1)
	}
}

func getAppName(branch string) (string, error) {
	split := strings.Split(branch, "/")
	if len(split) != 2 {
		return "", errors.New("Wrong format of brnach name should be inform release/appname")
	}

	return split[1], nil
}

func getBump(branch string) (string, error) {
	bump := "minor"
	split := strings.Split(branch, "/")
	if len(split) != 2 {
		return "", errors.New("Wrong format of brnach name should be inform release/appname")
	}

	if split[0] == "staging" {
		bump = "patch"
	}

	return bump, nil
}

func getRelease(branch string) (string, error) {
	staging := "production"
	split := strings.Split(branch, "/")
	if len(split) != 2 {
		return "", errors.New("Wrong format of brnach name should be inform release/appname")
	}

	if split[0] == "staging" {
		staging = "staging"
	}

	return staging, nil
}

func getConfiguration(appName string) *appConfig {
	item := fmt.Sprintf("configs/%s/config.yml", appName)
	file, err := os.Create("config.yml")
	if err != nil {
		log.Fatalf("Unable to open file %q, %v", item, err)
	}

	downloadfile(item, file)
	yamlFile, err := ioutil.ReadFile("config.yml")
	if err != nil {
		log.Printf("yamlFile.Get err   #%v ", err)
	}

	appC := &appConfig{}
	err = yaml.Unmarshal(yamlFile, appC)
	if err != nil {
		log.Fatalf("Unmarshal: %v", err)
	}

	return appC
}

func getKeystoreCredentials(appName string) (string, string, string) {
	item := "keystore/keystore.properties"
	file, err := os.Create("keystore.properties")
	if err != nil {
		log.Fatalf("Unable to open file %q, %v", item, err)
	}

	downloadfile(item, file)

	viper.SetConfigName("keystore")
	viper.AddConfigPath(".")
	err = viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s", err))
	}

	storePassword := viper.GetString("storePassword")
	password := viper.GetString(fmt.Sprintf("%sPassword", appName))
	alias := viper.GetString(fmt.Sprintf("%sAlias", appName))

	return storePassword, alias, password
}

func downloadfile(item string, file *os.File) {
	sess := session.Must(session.NewSession(
		&aws.Config{
			Region: aws.String(os.Getenv("AWS_REGION")),
			Credentials: credentials.NewStaticCredentials(
				os.Getenv("AWS_ACCESS_KEY_ID"),
				os.Getenv("AWS_SECRET_ACCESS_KEY"),
				"",
			),
		}),
	)

	downloader := s3manager.NewDownloader(sess)
	_, err := downloader.Download(file,
		&s3.GetObjectInput{
			Bucket: aws.String(os.Getenv("AWS_S3_BUCKET")),
			Key:    aws.String(item),
		})

	if err != nil {
		log.Fatalf("Unable to download item %q, %v", item, err)
	}
}
